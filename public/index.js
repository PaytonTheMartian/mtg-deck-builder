// Variables
const api = 'https://api.scryfall.com'
var cards = []
var cache = {}

// One-liner functions :cool:
const sleep = ms => new Promise(r => setTimeout(r, ms))
const titlecase = s =>
	s.split(' ').map(v => v.charAt(0).toUpperCase() + v.slice(1, v.length)).join(' ')

// Get a card by it's name
const getCard = async function (c)
{
	c = c.toLowerCase()

	if (c in cache)
		return { card: cache[c], wasCached: true }

	console.debug(`Caching \`${c}\``)

	const response = await fetch(api + `/cards/named?exact=${c}`)
	cache[c] = await response.json()

	return { card: cache[c], wasCached: false }
}

// Removes a card from the current deck by it's id
const removeCard = function (index)
{
	cards = cards.filter((_v, i) => i !== index)
	updateCardList()
}

// Updates the decklist
const updateCardList = async function()
{
	document.getElementById('decklist-code').value = cards.join('§')
	document.getElementById('decklist-export').value = cards.map((v, i) => `${i+1}. ${titlecase(v)}`).join('\n')
	document.getElementById('decklist-title').innerText = `Decklist: (${cards.length} cards)`
	document.getElementById('scryfall-query').innerText = cards.map(v => `!\"${v}\"`).join(' OR ')

	const dl = document.getElementById('decklist')
	dl.innerHTML = ''

	for (var i in cards)
	{
		var data = await getCard(cards[i])
		var c = data.card

		dl.innerHTML += `<li>(<span onclick='removeCard(${i})' style='cursor:pointer'>X</span>) <a href='${c.scryfall_uri}'>${c.name}</a></li>`

		// This is to honor Scryfall's API rate-limit.
		if (!data.wasCached)
			await sleep(100)

		i++
	}
}

// Loads each card from a decklist code
const loadCardsFromDecklistCode = function()
{
	const code = document.getElementById('decklist-code').value
	cards = code.split('§')
	updateCardList()
}

// Add a card to the current decklist
const addCard = function()
{
	const d = document.getElementById('add-card-input')
	if (d.value === '')
		return

	cards.push(d.value.toLowerCase())
	updateCardList()
	d.value = ''
}

const main = function()
{
	updateCardList()
}
